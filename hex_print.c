#include <stdio.h>

/*
How to print integers as hexadecimals. 
Author: Elizabeth Berg
*/

int main(void) {
	
	int x = 10;
	
	printf("%d\n", x);
	printf("%X\n", x); 
	printf("%#X\n", x); /* with a hashtag */
	printf("%#x\n", x); /* in lower case */
	
	return 0;
}