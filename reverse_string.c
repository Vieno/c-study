#include <stdio.h>
#include <string.h>

/*
Author: Elizabeth Berg
*/

int main(void) {
	
	char name[20];
	
	printf("What's your name?\nI am: ");
	scanf("%19s", name);
	
	printf("!");
	for (int i = strlen(name) - 1; i >= 0; i--) {
		printf("%c", name[i]);
	}
	printf(" olleH\n");
	
	return 0;
	
}