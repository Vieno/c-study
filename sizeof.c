#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

/*
Simple print program to test user's understanding of sizeof. Read the code block
and answer questions in print block.
Sizeof is an unary operator. Other unary operators include *, % and ?.
Author: Elizabeth Berg
*/

int main(void) {
	
	//Code block.
	int x = 7;
	double y = 0.4;

	printf("Please consider the following examples.\n\n");

	printf("sizeof(x) returns %zu, when int x = 7\n", sizeof(x));
	printf("sizeof(y) returns %zu, when double y = -0.4\n", sizeof(y));
	printf("sizeof(-35) returns %zu\n", sizeof(-35));
	printf("sizeof(2 147 483 647) returns %zu\n\n", sizeof(2147483647));
	
	//Print block.
	printf("Now select the correct answer.\n\n");
	
	printf("1: Variable's size in memory depends on variable's value.\n");
	printf("2: Variable's size in memory depends on variable's data type.\n");
	printf("3: Sizeof returns the same amount of bytes every time.\nAnswer: ");
	
	int answer;
	scanf("%d", &answer);
	
	switch(answer) {
		case 1:
			printf("Wrong.");
			break;
		case 2:
			printf("Correct.");
			break;
		case 3:
			printf("Wrong.");
			break;
		default:
			printf("Please select a number between 1 and 3.");
	}
	
	return 0;
}



