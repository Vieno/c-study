#include <stdio.h>
#include <stdlib.h>

/*
Computer learns how to print users' names.
Author: Elizabeth Berg
*/

int main(void) {
	
	char name[20];
	
	printf("Begin by entering your name.\nYou: ");
	scanf("%19s", name);
	
	printf("Computer: What a beautiful name. Let's try and print it then.\n");
	printf("Hello, name.\n");
	printf("...\nComputer: It didn't work. Why?\n");
	printf("Computer: Now I know. C uses %%s to print strings.\n");
	
	printf("Hello, %s!\n", name);
	
	return 0;
	
}