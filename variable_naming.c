/*
Simple program to test naming rules. 
Author: Elizabeth Berg
*/

int main(void) {
	
	/* Legal */
	char* _down_under = "Australia";
	
	/* Illegal */
	int 999god;
	int #life;
	char Ben&Jerry;
	char* Scarlett*Rhett = "Love";
	
	return 0;
	
}